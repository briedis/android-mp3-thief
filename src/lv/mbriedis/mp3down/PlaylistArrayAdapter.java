package lv.mbriedis.mp3down;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import lv.mbriedis.mp3down.db.Playlist;

import java.util.List;

public class PlaylistArrayAdapter extends ArrayAdapter<Playlist> {
    private final Context context;
    private List<Playlist> values;

    public PlaylistArrayAdapter(Context context, List<Playlist> values) {
        super(context, R.layout.song_list_item, values);
        this.context = context;
        this.values = values;
    }

    public void setValues(List<Playlist> values) {
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.playlist_list_item, parent, false);

        Playlist playlist = values.get(position);

        TextView textView = (TextView) rowView.findViewById(R.id.textViewPlaylistTitle);
        textView.setText(playlist.getName());

        return rowView;
    }
}
