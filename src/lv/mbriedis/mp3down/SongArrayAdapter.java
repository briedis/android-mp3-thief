package lv.mbriedis.mp3down;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import lv.mbriedis.mp3down.db.Song;

import java.util.ArrayList;
import java.util.List;

public class SongArrayAdapter extends ArrayAdapter<Song> {
    private final Context context;
    private List<Song> values;

    private final List<Integer> progressVisible = new ArrayList<Integer>();

    public SongArrayAdapter(Context context, List<Song> values) {
        super(context, R.layout.song_list_item, values);
        this.context = context;
        this.values = values;
    }

    public void setValues(List<Song> values) {
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.song_list_item, parent, false);

        Song song = values.get(position);

        TextView textView = (TextView) rowView.findViewById(R.id.label);
        textView.setText(song.getName());

        TextView textViewSpecial = (TextView) rowView.findViewById(R.id.labelFormatted);

        int count = song.getUrls().size();
        textViewSpecial.setText(count > 0 ? count + " URLs found" : "No URLs found");

        ProgressBar bar = (ProgressBar) rowView.findViewById(R.id.progressBarSong);
        bar.setVisibility(progressVisible.contains(position) ? View.VISIBLE : View.INVISIBLE);

        return rowView;
    }

    public void setProgressVisible(Integer position, boolean state) {
        if (state) {
            progressVisible.add(position);
        } else if (progressVisible.contains(position)) {
            progressVisible.remove(position);
        }
    }
}
