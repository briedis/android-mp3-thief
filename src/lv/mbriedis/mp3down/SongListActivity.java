package lv.mbriedis.mp3down;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.j256.ormlite.dao.Dao;
import lv.mbriedis.mp3down.db.DatabaseHelper;
import lv.mbriedis.mp3down.db.Playlist;
import lv.mbriedis.mp3down.db.Song;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SongListActivity extends ListActivity {
    public static String TAG = "APP";

    final Context context = this;
    private SongArrayAdapter adapter;
    private Playlist playlist;
    private DatabaseHelper dbHelper;

    private static final int REQUEST_ADD_SONG = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelper = new DatabaseHelper(getApplicationContext());

        int playlistId = getIntent().getIntExtra("playlistId", 0);
        if (playlistId > 0) {
            try {
                playlist = dbHelper.getPlaylistDao().queryForId(playlistId);
            } catch (SQLException e) {
                e.printStackTrace();
                Log.e(TAG, "Failed to load playlist!");
            }
        }

        setTitle(playlist.getName());

        refreshSongList();

        // Context menu
        registerForContextMenu(getListView());
    }

    @Override
    protected void onStop() {
        super.onStop();
        Player.stop();
    }

    private void refreshSongList() {
        try {
            playlist = dbHelper.getPlaylistDao().queryForId(playlist.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(TAG, "Failed to reload playlist!");
        }

        Collection<Song> songsColl = playlist.getSongs();
        final List<Song> songs = songsColl instanceof List ? (List<Song>) songsColl : new ArrayList<Song>(songsColl);
        Collections.reverse(songs);

        if (adapter != null && songs.size() != adapter.getCount()) {
            adapter = null;
        }

        if (adapter == null) {
            adapter = new SongArrayAdapter(this, songs);
            setListAdapter(adapter);
        } else {
            adapter.setValues(songs);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Song song = (Song) getListAdapter().getItem(position);
        Intent intent = new Intent(getApplicationContext(), SongUrlListActivity.class);
        intent.putExtra("songId", song.getId());
        startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.song_context, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.song_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addNewSong:
                newSong();
                return true;
            case R.id.viewPlaylists:
                viewPlaylists();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void viewPlaylists() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), PlaylistActivity.class);
        startActivity(intent);
    }

    private void newSong() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), AddSongActivity.class);
        intent.putExtra("playlistId", playlist.getId());
        startActivityForResult(intent, REQUEST_ADD_SONG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // When song was added
        if (requestCode == REQUEST_ADD_SONG && resultCode == Activity.RESULT_OK) {
            refreshSongList();
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int position = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;

        switch (item.getItemId()) {
            case R.id.delete:
                deleteSongPrompt(position);
                return true;
            case R.id.loadUrls:
                findSongURLs(position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteSongPrompt(final int position) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.song_delete_prompt, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);


        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    Dao<Song, Integer> dao = dbHelper.getDao(Song.class);
                                    dao.delete((Song) getListAdapter().getItem(position));
                                    Toast.makeText(context, "Song deleted", Toast.LENGTH_SHORT).show();
                                } catch (SQLException e) {
                                    Toast.makeText(context, "Failed to delete song!", Toast.LENGTH_SHORT).show();
                                    Log.e(TAG, e.toString());
                                    e.printStackTrace();
                                }
                                // Refresh list
                                refreshSongList();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void findSongURLs(final int position) {
        adapter.setProgressVisible(position, true);
        adapter.notifyDataSetChanged();

        UrlFinder finder = new UrlFinder(adapter.getItem(position), this);

        finder.startFinder(new Runnable() {
            @Override
            public void run() {
                adapter.setProgressVisible(position, false);
                refreshSongList();
            }
        });
    }
}
