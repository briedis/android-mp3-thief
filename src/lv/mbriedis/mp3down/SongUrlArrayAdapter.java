package lv.mbriedis.mp3down;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import lv.mbriedis.mp3down.db.SongUrl;

import java.util.List;

public class SongUrlArrayAdapter extends ArrayAdapter<SongUrl> {
    private final String TAG = "APP";
    private final Context context;
    private final List<SongUrl> values;


    public SongUrlArrayAdapter(SongUrlListActivity activity, List<SongUrl> values) {
        super(activity, R.layout.url_list_item, values);
        this.values = values;
        this.context = activity.getApplicationContext();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.url_list_item, parent, false);

        SongUrl url = values.get(position);

        TextView textView = (TextView) rowView.findViewById(R.id.textViewUrlTitle);
        textView.setText((position + 1) + ". " + url.getTitle());

        TextView textViewSpecial = (TextView) rowView.findViewById(R.id.textViewUrl);
        textViewSpecial.setText(url.getUrl());

        return rowView;
    }
}
