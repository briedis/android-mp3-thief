package lv.mbriedis.mp3down;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import lv.mbriedis.mp3down.db.DatabaseHelper;
import lv.mbriedis.mp3down.db.Song;
import lv.mbriedis.mp3down.db.SongUrl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SongUrlListActivity extends Activity {
    private final String TAG = "APP";
    private ListView urlListView;
    private Activity activity;
    private Song song;
    private DatabaseHelper dbHelper;
    private MenuItem menuItemRefresh;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        dbHelper = new DatabaseHelper(getApplicationContext());

        int songId = this.getIntent().getIntExtra("songId", 0);

        try {
            song = dbHelper.getSongDao().queryForId(songId);
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        }

        if (song == null) {
            return;
        }

        setContentView(R.layout.url_list);
        this.setTitle(song.getName());

        urlListView = (ListView) findViewById(R.id.listViewSongUrls);

        refreshListView();

        // Open streaming/download dialog when clicked on an item
        urlListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), StreamActivity.class);

                SongUrl songUrl = (SongUrl) urlListView.getAdapter().getItem(position);
                intent.putExtra("songUrlId", songUrl.getId());

                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.song, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                menuItemRefresh = item;
                refreshUrls();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void refreshListView() {
        try {
            song = dbHelper.getSongDao().queryForId(song.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

        Collection<SongUrl> urlsColl = song.getUrls();
        final List<SongUrl> urls = urlsColl instanceof List ? (List<SongUrl>) urlsColl : new ArrayList<SongUrl>(urlsColl);
        urlListView.setAdapter(new SongUrlArrayAdapter(this, urls));
    }

    private void refreshUrls() {
        Toast.makeText(getApplicationContext(), R.string.buttonReloadUrlsLoading, Toast.LENGTH_LONG).show();
        menuItemRefresh.setTitle(R.string.buttonReloadUrlsLoading);
        (new UrlFinder(song, activity)).startFinder(new Runnable() {
            @Override
            public void run() {
                refreshListView();
                menuItemRefresh.setTitle(R.string.refresh);
            }
        });
    }
}