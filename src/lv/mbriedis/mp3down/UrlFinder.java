package lv.mbriedis.mp3down;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.j256.ormlite.dao.Dao;
import lv.mbriedis.mp3down.db.DatabaseHelper;
import lv.mbriedis.mp3down.db.Song;
import lv.mbriedis.mp3down.db.SongUrl;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class UrlFinder {
    private final String TAG = "APP";
    private Song song = null;
    private Context context = null;
    private Activity activity = null;

    private Runnable callback = null;

    public UrlFinder(Song song, Activity activity) {
        this.song = song;
        this.activity = activity;
        this.context = activity.getApplicationContext();
    }

    private String getUrl() {
        String name = "";
        try {
            name = URLEncoder.encode(song.getNameFormatted(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "http://mp3skull.com/mp3/" + name + ".html";
    }

    public void startFinder(Runnable callback) {
        this.callback = callback;
        new GetHtmlTask().execute(song);
    }

    /**
     * Parse HTML, get urls for songs and save them in the database
     *
     * @param html HTML containing links to song download
     */
    private void resolveAndStoreUrls(String html) {
        // Parse html, get URL and store them to this Song
        Log.i(TAG, "Resolve and store Urls for Song: " + song.getName());

        DatabaseHelper helper = new DatabaseHelper(context);

        HashMap<String, String> urls = new HashMap<String, String>();

        // Parse out URLs and titles
        Document doc = Jsoup.parse(html);
        Elements els = doc.getElementsByAttributeValue("id", "right_song");
        for (Element el : els) {
            String title = el.childNode(1).childNode(0).childNode(0).toString();
            String url = el.childNode(5).childNode(1).childNode(1).childNode(0).attr("href");
            urls.put(url, title);
        }


        int count = 0;

        Dao<SongUrl, Integer> dao = null;

        try {
            dao = helper.getSongUrlDao();
        } catch (SQLException e) {
            Log.e(TAG, "Song url dao failed!");
        }

        if (dao == null) {
            return;
        }

        for (Map.Entry<String, String> entry : urls.entrySet()) {
            try {
                SongUrl songUrl = new SongUrl();
                songUrl.setSong(song);
                songUrl.setUrl(entry.getKey());
                songUrl.setTitle(entry.getValue());
                dao.create(songUrl);
                count += 1;
            } catch (SQLException e) {
                Log.e(TAG, "Song url (" + entry.getKey() + ") insert failed: " + e.getMessage());
            }
        }

        final int totalCount = count;
        activity.runOnUiThread(new Runnable() {
            private int count = totalCount;

            @Override
            public void run() {
                Toast.makeText(context, "Found " + count + " URLs for \"" + song.getName() + "\"", Toast.LENGTH_LONG).show();
            }
        });

        Log.i(TAG, "Stored " + count + " URLs for \"" + song.getName() + "\"");
    }

    /**
     * Task that fetches html and sends it to URL parsing
     */
    private class GetHtmlTask extends AsyncTask<Song, Integer, String> {
        protected String doInBackground(Song[] song) {
            Log.i(TAG, "GetHtmlTask thread: " + Thread.currentThread().getName());
            String html = Utils.getHtml(getUrl());
            resolveAndStoreUrls(html);
            // This return is symbolic, all the stuff happens in resolveAndStoreUrls
            return html;
        }

        protected void onPostExecute(String html) {
            if (callback != null) {
                callback.run();
            }
        }
    }
}

