package lv.mbriedis.mp3down.db;

import android.net.Uri;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "urls")
public class SongUrl {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true, uniqueCombo = true)
    private Song song;

    @DatabaseField(uniqueCombo = true)
    private String url;

    @DatabaseField
    private String title;

    public SongUrl() {
        // ORMLite needs a no-arg constructor
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public Uri getUri() {
        return Uri.parse(url.replace(" ", "%20"));
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        title = title.replace("mp3", "");
        this.title = title;
    }
}